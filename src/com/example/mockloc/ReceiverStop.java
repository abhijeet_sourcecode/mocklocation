package com.example.mockloc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ReceiverStop extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("example", "broadcast recevied in ReceiverStop," + Global.isMocking);
		if (Global.isMocking) {
			Global.stopMocking();
		}

	}

}
