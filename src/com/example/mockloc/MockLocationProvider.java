package com.example.mockloc;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;

public class MockLocationProvider {
	String providerName;
	Context ctx;

	public MockLocationProvider(String name, Context ctx) {
		this.providerName = name;
		this.ctx = ctx;

		LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
		lm.addTestProvider(providerName, false, false, false, false, false, true, true, 0, 5);
		lm.setTestProviderEnabled(providerName, true);
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	@SuppressLint("NewApi")
	public void pushLocation(double lat, double lon) {
		LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
		Log.i("example", "lat " + lat + ", long " + lon);
		Location mockLocation = new Location(providerName);
		mockLocation.setLatitude(lat);
		mockLocation.setLongitude(lon);
		mockLocation.setAltitude(0);
		mockLocation.setTime(System.currentTimeMillis());
		mockLocation.setAccuracy(1);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			mockLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
		}
		lm.setTestProviderLocation(providerName, mockLocation);
	}

	public void shutdown() {
		Log.i("example", "shutting down mock location provider");
		LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
		lm.removeTestProvider(providerName);
	}
}