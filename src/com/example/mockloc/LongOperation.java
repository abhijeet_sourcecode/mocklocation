package com.example.mockloc;

import android.os.AsyncTask;
import android.util.Log;

public class LongOperation extends AsyncTask<String, Void, String> {

	private static final double LATMAX = 180;// should be 90, but we set it to
												// 360
	private static final double LONGMAX = 360;// should be 180, but we set it to
												// 360
	private long SLEEPTIME = 100; // milliseconds

	@Override
	protected String doInBackground(String... params) {
		changeLoc();
		return "mock location task completed";
	}

	protected void changeLoc() {

		for (double latitude = 0; latitude <= LATMAX; latitude += 5) {
			for (double longitude = 0; longitude <= LONGMAX; longitude += 10) {
				Global.mock.pushLocation(latitude, longitude);
				try {
					Thread.sleep(SLEEPTIME);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	protected void onPostExecute(String result) {
		Log.i("example", "mock location task completed");
	}

	@Override
	protected void onPreExecute() {

	}

	@Override
	protected void onProgressUpdate(Void... values) {
	}

}