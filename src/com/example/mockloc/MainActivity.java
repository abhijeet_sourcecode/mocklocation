package com.example.mockloc;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	private Button startBtn;
	private Button stopBtn;
	private TextView textview;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textview = (TextView) findViewById(R.id.textView1);
		if (Global.isMocking) {
			textview.setText("Mocking location enabled");
		} else {
			textview.setText("Mocking location disabled");
		}

		final Context context = this;

		startBtn = (Button) findViewById(R.id.button1);
		startBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!Global.isMocking) {
					Global.startMocking(context);
					textview.setText("Mocking location enabled");
				}
			}
		});

		stopBtn = (Button) findViewById(R.id.button2);
		stopBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (Global.isMocking) {
					Global.stopMocking();
					textview.setText("Mocking location disabled");
				}

			}
		});
	}

	protected void onDestroy() {
		Global.stopMocking();
		super.onDestroy();
	}
}