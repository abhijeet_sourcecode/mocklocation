package com.example.mockloc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ReceiverStart extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("example", "broadcast recevied in ReceiverStart," + Global.isMocking);
		if (!Global.isMocking) {
			Global.startMocking(context);
		}

	}

}
