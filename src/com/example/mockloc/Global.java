package com.example.mockloc;

import android.content.Context;
import android.location.LocationManager;
import android.os.AsyncTask;

public class Global {

	static AsyncTask<String, Void, String> ops;
	static boolean isMocking = false;
	static MockLocationProvider mock = null;

	static void startMocking(Context context) {
		if (Global.mock == null) {
			Global.mock = new MockLocationProvider(LocationManager.GPS_PROVIDER, context);
		}
		Global.isMocking = true;
		Global.ops = new LongOperation().execute("");

	}

	static void stopMocking() {
		Global.ops.cancel(true);
		Global.mock.shutdown();
		Global.isMocking = false;
		Global.mock = null;
	}

}
